import React from 'react';
import './Form.css';

class ControledComponenet extends React.Component {
    constructor(){
        super();
        this.state = {
            name : '',
            salary : '',
            gender : 'l',
            skill : [],
            address : ''
        }
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onPick = this.onPick.bind(this);
        this.onCheck = this.onCheck.bind(this);
    }

    onChange(event) {
        const {name, value} = event.target;
        this.setState({[name]: value});        
    }

    onSubmit(event) {
        event.preventDefault();
        alert(`hi nama saya ${this.state.name}`);
        console.log('ini namanya ', this.state.name);
    }

    onCheck(event) {
        const {name, value} = event.target;
        this.setState({[name]: value});
        console.log('radio ', this.state.gender);
        
    }

    onPick(event) {
        console.log(event.target.value);
        let skill = [...this.state.skill];
        let idx = skill.findIndex(element => event.target.value === element);
        if (idx > 0) {
            skill = [...skill.slice(0, idx), ...skill.slice(idx + 1, skill.length)]
        } else if (idx === 0){
            skill = [...skill.slice(idx + 1, skill.length)]
        } else {
            skill.push(event.target.value);
        }
        this.setState({skill: event.target.value});
    }

    state = { 
        selectedFile: null
      }; 
       
      onFileChange = event => { 
        this.setState({ selectedFile: event.target.files[0] }); 
      }; 
      onFileUpload = () => { 
        const formData = new FormData(); 
        formData.append( 
          "myFile", 
          this.state.selectedFile, 
          this.state.selectedFile.name 
        ); 
        console.log(this.state.selectedFile); 

        axios.post("api/uploadfile", formData); 
      }; 

    render() {
        const {name, salary, skill, address} = this.state;
        return(
            <>
                <h2>Ini Form Controlled</h2>
                <form className='form' onSubmit={this.onSubmit}>
                    <label>Nama</label>
                    <input name='name' type='text' value={name} onChange={this.onChange}/>
                    <label>Salary</label>
                    <input name='salary' type='number' value={salary} onChange={this.onChange}/>
                    <label>Gender</label>
                    <label><input name='gender' type='radio' value='l' onChange={this.onCheck}/>Laki-laki</label>
                    <label><input name='gender' type='radio' value='p' onChange={this.onCheck}/>Perempuan</label>
                    <label>Skill</label>
                    <select multiple={true} value={skill} onChange={this.onPick}>
                        <option value='javascript'>Javascript</option>
                        <option value='php'>PHP</option>
                        <option value='java'>Java</option>
                    </select>
                    <label>Address</label>
                    <textarea name='address' value={address} onChange={this.onChange}/>
                    <input type="file" onChange={this.onFileChange} />
                    <button onClick={this.onFileUpload}>Upload!</button> 
                    <input type='submit'/>
                </form>
                <br/>
             </>
        );
    }
}
export default ControledComponenet;